package com.kelompokppb.ppb2122

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class login_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val textCreate = findViewById<TextView>(R.id.txt_create)
        textCreate.setOnClickListener{
            val intent = Intent(this, regis_Activity::class.java)
            startActivity(intent)
        }
    }
}